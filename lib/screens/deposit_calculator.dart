import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pattern_formatter/pattern_formatter.dart';

class DepositCalculator extends StatefulWidget {
  @override
  _DepositCalculatorState createState() => _DepositCalculatorState();
}

class _DepositCalculatorState extends State<DepositCalculator> {

  int stolenCatPercentage = 0;
  int nestCatPercentage = 0;
  int grassCatPercentage = 0;
  int technoCatPercentage = 0;
  int pillowCatPercentage = 0;
  final amountController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Theme
            .of(context)
            .backgroundColor,
        appBar: AppBar(
          title: Center(
            child: Text(
              'Калькулятор Зверинца',
              style: TextStyle(
                fontSize: 28.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          elevation: 0.0,
        ),
        body: Container(
          alignment: Alignment.center,
          child: ListView(
            scrollDirection: Axis.vertical,
            children: <Widget>[
              Center(
                child: Container(
                  width: MediaQuery
                      .of(context)
                      .size
                      .width - 25,
                  margin: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
                  child: TextField(
                    controller: amountController,
                    keyboardType: TextInputType.numberWithOptions(
                        decimal: true
                    ),
                    style: TextStyle(
                      fontSize: 25,
                      color: Theme
                          .of(context)
                          .primaryColor,
                    ),
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.attach_money),
                        suffixIcon: IconButton(
                          icon: Icon(Icons.clear),
                          onPressed: () {
                            amountController.clear();
                            this.setState(() {
                              stolenCatPercentage = 0;
                              nestCatPercentage = 0;
                              grassCatPercentage = 0;
                              technoCatPercentage = 0;
                              pillowCatPercentage = 0;
                            });
                          }
                        ),
                        labelText: "Сколько принес?",
                        floatingLabelBehavior: FloatingLabelBehavior.auto,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                            gapPadding: 10
                        )
                    ),
                    onChanged: (String value) {
                      double amount = double.parse(value);
                      setState(() {
                        stolenCatPercentage = (amount * 0.2429).ceil();
                        nestCatPercentage = (amount * 0.3205).ceil();
                        grassCatPercentage = (amount * 0.1486).ceil();
                        technoCatPercentage = (amount * 0.0579).ceil();
                        pillowCatPercentage = (amount * 0.1).ceil();
                      });
                    },
                  ),
                ),
              ),
              Container(
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                margin: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Theme.of(context).backgroundColor,
                  borderRadius: BorderRadius.circular(30),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10.0),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Кредиты: ",
                              style: TextStyle(
                                color: Colors.white
                              ),),
                            Text(stolenCatPercentage.toString())
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Квартира: "),
                            Text(nestCatPercentage.toString())
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Еда: "),
                            Text(grassCatPercentage.toString())
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Коммуникации: "),
                            Text(technoCatPercentage.toString())
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Подушка: "),
                            Text(pillowCatPercentage.toString())
                          ],
                        ),
                      )
                    ],

                  ),
                ),
              )
            ],
          ),
          decoration: BoxDecoration(
              color: Theme
                  .of(context)
                  .canvasColor,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30.0),
                topRight: Radius.circular(30.0),
              )
          ),
        ),
      ),
    );
  }
}
