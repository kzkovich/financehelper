import 'package:FinanceHelper/utils/color_extension.dart';
import 'package:flutter/material.dart';

import 'screens/deposit_calculator.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Финансовый калькулятор',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: HexColor("#0277BD"),
        backgroundColor: HexColor("#0277BD"),
        canvasColor: Colors.blue.shade50,
        accentColor: Colors.blue.shade600,
        textTheme: TextTheme(
          bodyText2: TextStyle(
            color: Colors.white,
            fontSize: 24.0,
            fontWeight: FontWeight.w600,
            letterSpacing: 1.3,
          )
        )
      ),
      home: DepositCalculator(),
    );
  }
}